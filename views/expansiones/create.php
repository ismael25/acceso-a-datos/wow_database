<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Expansiones */

$this->title = 'Create Expansiones';
$this->params['breadcrumbs'][] = ['label' => 'Expansiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expansiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
