<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tienen */

$this->title = 'Create Tienen';
$this->params['breadcrumbs'][] = ['label' => 'Tienens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
