<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mazmorras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mazmorras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mazmorras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'ubicacion',
            'num_boses',
            'nivel_requerido',
            //'id_expansiones',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
