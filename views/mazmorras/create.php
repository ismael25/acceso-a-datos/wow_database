<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mazmorras */

$this->title = 'Create Mazmorras';
$this->params['breadcrumbs'][] = ['label' => 'Mazmorras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mazmorras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
