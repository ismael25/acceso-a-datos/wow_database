<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Protagonistas */

$this->title = 'Create Protagonistas';
$this->params['breadcrumbs'][] = ['label' => 'Protagonistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="protagonistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
