<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aparecen */

$this->title = 'Create Aparecen';
$this->params['breadcrumbs'][] = ['label' => 'Aparecens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aparecen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
