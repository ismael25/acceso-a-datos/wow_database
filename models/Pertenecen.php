<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenecen".
 *
 * @property int $id
 * @property int|null $id_razas
 * @property int|null $id_expansiones
 *
 * @property Expansiones $expansiones
 * @property Razas $razas
 */
class Pertenecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_razas', 'id_expansiones'], 'integer'],
            [['id_expansiones', 'id_razas'], 'unique', 'targetAttribute' => ['id_expansiones', 'id_razas']],
            [['id_expansiones'], 'exist', 'skipOnError' => true, 'targetClass' => Expansiones::className(), 'targetAttribute' => ['id_expansiones' => 'id']],
            [['id_razas'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_razas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_razas' => 'Id Razas',
            'id_expansiones' => 'Id Expansiones',
        ];
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasOne(Expansiones::className(), ['id' => 'id_expansiones']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasOne(Razas::className(), ['id' => 'id_razas']);
    }
}
