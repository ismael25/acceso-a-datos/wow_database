<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roles".
 *
 * @property int $id
 * @property int|null $id_clases
 * @property string|null $roles
 *
 * @property Roles $clases
 * @property Roles[] $roles0
 */
class Roles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clases'], 'integer'],
            [['roles'], 'string', 'max' => 20],
            [['id_clases', 'roles'], 'unique', 'targetAttribute' => ['id_clases', 'roles']],
            [['id_clases'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['id_clases' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clases' => 'Id Clases',
            'roles' => 'Roles',
        ];
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasOne(Roles::className(), ['id' => 'id_clases']);
    }

    /**
     * Gets query for [[Roles0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoles0()
    {
        return $this->hasMany(Roles::className(), ['id_clases' => 'id']);
    }
}
