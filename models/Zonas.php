<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $num_regiones
 * @property int|null $id_expansiones
 *
 * @property Zonas $expansiones
 * @property Zonas[] $zonas
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_regiones', 'id_expansiones'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['id_expansiones'], 'exist', 'skipOnError' => true, 'targetClass' => Zonas::className(), 'targetAttribute' => ['id_expansiones' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'num_regiones' => 'Num Regiones',
            'id_expansiones' => 'Id Expansiones',
        ];
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasOne(Zonas::className(), ['id' => 'id_expansiones']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zonas::className(), ['id_expansiones' => 'id']);
    }
}
