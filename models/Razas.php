<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $afiliacion
 * @property string|null $descripcion
 * @property string|null $racial
 *
 * @property Pertenecen[] $pertenecens
 * @property Expansiones[] $expansiones
 * @property Tienen[] $tienens
 * @property Clases[] $clases
 */
class Razas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'afiliacion', 'racial'], 'string', 'max' => 50],
            [['descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'afiliacion' => 'Afiliacion',
            'descripcion' => 'Descripcion',
            'racial' => 'Racial',
        ];
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::className(), ['id_razas' => 'id']);
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasMany(Expansiones::className(), ['id' => 'id_expansiones'])->viaTable('pertenecen', ['id_razas' => 'id']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['id_razas' => 'id']);
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['id' => 'id_clase'])->viaTable('tienen', ['id_razas' => 'id']);
    }
}
