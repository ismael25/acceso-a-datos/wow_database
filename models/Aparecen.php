<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aparecen".
 *
 * @property int $id
 * @property int|null $id_protagonistas
 * @property int|null $id_expansiones
 *
 * @property Expansiones $expansiones
 * @property Protagonistas $protagonistas
 */
class Aparecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aparecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_protagonistas', 'id_expansiones'], 'integer'],
            [['id_expansiones', 'id_protagonistas'], 'unique', 'targetAttribute' => ['id_expansiones', 'id_protagonistas']],
            [['id_expansiones'], 'exist', 'skipOnError' => true, 'targetClass' => Expansiones::className(), 'targetAttribute' => ['id_expansiones' => 'id']],
            [['id_protagonistas'], 'exist', 'skipOnError' => true, 'targetClass' => Protagonistas::className(), 'targetAttribute' => ['id_protagonistas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_protagonistas' => 'Id Protagonistas',
            'id_expansiones' => 'Id Expansiones',
        ];
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasOne(Expansiones::className(), ['id' => 'id_expansiones']);
    }

    /**
     * Gets query for [[Protagonistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProtagonistas()
    {
        return $this->hasOne(Protagonistas::className(), ['id' => 'id_protagonistas']);
    }
}
