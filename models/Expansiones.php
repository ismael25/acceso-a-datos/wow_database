<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expansiones".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $rango_niveles
 * @property string|null $fecha
 *
 * @property Aparecen[] $aparecens
 * @property Protagonistas[] $protagonistas
 * @property Pertenecen[] $pertenecens
 * @property Razas[] $razas
 */
class Expansiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expansiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['nombre', 'rango_niveles'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'rango_niveles' => 'Rango Niveles',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Aparecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAparecens()
    {
        return $this->hasMany(Aparecen::className(), ['id_expansiones' => 'id']);
    }

    /**
     * Gets query for [[Protagonistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProtagonistas()
    {
        return $this->hasMany(Protagonistas::className(), ['id' => 'id_protagonistas'])->viaTable('aparecen', ['id_expansiones' => 'id']);
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::className(), ['id_expansiones' => 'id']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasMany(Razas::className(), ['id' => 'id_razas'])->viaTable('pertenecen', ['id_expansiones' => 'id']);
    }
}
