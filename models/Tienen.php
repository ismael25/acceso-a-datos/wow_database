<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property int|null $id_razas
 * @property int|null $id_clase
 *
 * @property Clases $clase
 * @property Razas $razas
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_razas', 'id_clase'], 'integer'],
            [['id_clase', 'id_razas'], 'unique', 'targetAttribute' => ['id_clase', 'id_razas']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id']],
            [['id_razas'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_razas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_razas' => 'Id Razas',
            'id_clase' => 'Id Clase',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id' => 'id_clase']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasOne(Razas::className(), ['id' => 'id_razas']);
    }
}
