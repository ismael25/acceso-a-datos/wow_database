<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mazmorras".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $ubicacion
 * @property int|null $num_boses
 * @property int|null $nivel_requerido
 * @property int|null $id_expansiones
 *
 * @property Mazmorras $expansiones
 * @property Mazmorras[] $mazmorras
 */
class Mazmorras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mazmorras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_boses', 'nivel_requerido', 'id_expansiones'], 'integer'],
            [['nombre', 'ubicacion'], 'string', 'max' => 50],
            [['id_expansiones'], 'exist', 'skipOnError' => true, 'targetClass' => Mazmorras::className(), 'targetAttribute' => ['id_expansiones' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ubicacion' => 'Ubicacion',
            'num_boses' => 'Num Boses',
            'nivel_requerido' => 'Nivel Requerido',
            'id_expansiones' => 'Id Expansiones',
        ];
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasOne(Mazmorras::className(), ['id' => 'id_expansiones']);
    }

    /**
     * Gets query for [[Mazmorras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMazmorras()
    {
        return $this->hasMany(Mazmorras::className(), ['id_expansiones' => 'id']);
    }
}
