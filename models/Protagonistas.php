<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "protagonistas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $raza
 * @property string|null $afiliacion
 * @property string|null $estado
 * @property string|null $descripcion
 *
 * @property Aparecen[] $aparecens
 * @property Expansiones[] $expansiones
 */
class Protagonistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'protagonistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'raza', 'estado'], 'string', 'max' => 50],
            [['afiliacion', 'descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'raza' => 'Raza',
            'afiliacion' => 'Afiliacion',
            'estado' => 'Estado',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Aparecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAparecens()
    {
        return $this->hasMany(Aparecen::className(), ['id_protagonistas' => 'id']);
    }

    /**
     * Gets query for [[Expansiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpansiones()
    {
        return $this->hasMany(Expansiones::className(), ['id' => 'id_expansiones'])->viaTable('aparecen', ['id_protagonistas' => 'id']);
    }
}
